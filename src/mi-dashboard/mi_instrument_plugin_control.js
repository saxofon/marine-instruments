"use strict";

(function(){ 

function instrument_control_init(instrument)
{
	instrument.ctrl = new Array();

	// Instrument add
	instrument.ctrl.push(instrument.two.makeRoundedRectangle(instrument.width*0.5, instrument.height*0.2,
		instrument.width*0.9, instrument.height*0.2));
	instrument.ctrl[0].fill = "silver";
	instrument.ctrl.push(instrument.two.makeText("Instrument add", instrument.width*0.5, instrument.height*0.2));
	instrument.ctrl[1].size = instrument.size * 0.1;
	instrument.ctrl[1].fill = "black";

	// Instrument del
	instrument.ctrl.push(instrument.two.makeRoundedRectangle(instrument.width*0.5, instrument.height*0.4,
		instrument.width*0.9, instrument.height*0.2));
	instrument.ctrl[2].fill = "silver";
	instrument.ctrl.push(instrument.two.makeText("Instrument del", instrument.width*0.5, instrument.height*0.4));
	instrument.ctrl[3].size = instrument.size * 0.1;
	instrument.ctrl[3].fill = "black";

	// Simulator control
	instrument.ctrl.push(instrument.two.makeRoundedRectangle(instrument.width*0.5, instrument.height*0.6,
		instrument.width*0.9, instrument.height*0.2));
	instrument.ctrl[4].fill = "silver";
	instrument.ctrl.push(instrument.two.makeText("Simulator enable", instrument.width*0.5, instrument.height*0.6));
	instrument.ctrl[5].size = instrument.size * 0.1;
	instrument.ctrl[5].fill = "black";
}

function instrument_control_update(instrument)
{
	if (simulator_mode == 0) {
		instrument.ctrl[5].value = "Simulator enable";
		instrument.ctrl[4].fill = "silver";
	} else {
		instrument.ctrl[5].value = "Simulator disable";
		instrument.ctrl[4].fill = "red";
	}
	instrument.two.update();
}

function instrument_control_onclick(event)
{
	// check the generic instrument events
	instrument_onclick(me, event);

        // simulator events
        var area = me.instrument.ctrl[4].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (simulator_mode)
                        msg_mi.setSimulatorMode(0);
                else
                        msg_mi.setSimulatorMode(1);
                pbmsg = msg_mi.serializeBinary();
                ws.send(pbmsg);
                return;
        }

}

function instrument_control_onmousedown(event)
{
}

function instrument_control_onmouseup(event)
{
}

function instrument_control_onmousemove(event)
{
}

var me = {
        name : "Control",
        init : instrument_control_init,
        update : instrument_control_update,
        onclick : instrument_control_onclick,
        onmousedown : instrument_control_onmousedown,
        onmouseup : instrument_control_onmouseup,
        onmousemove : instrument_control_onmousemove,
};

mi_instrument_plugin.push(me);

})(); 

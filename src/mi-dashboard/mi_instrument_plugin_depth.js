"use strict";

(function(){

function instrument_depth_init(instrument)
{
	instrument.text_data = new Array();
	instrument.text_data.push(instrument.two.makeText("", instrument.width*0.5, instrument.height*6/20));
	instrument.text_data[0].size = instrument.size*0.5;
	instrument.text_data[0].fill = "black";

	instrument.text_data.push(instrument.two.makeText("", instrument.width*0.5, instrument.height*15/20));
	instrument.text_data[1].size = instrument.size*0.1;
	instrument.text_data[1].fill = "black";
}

function instrument_depth_update(instrument)
{
	instrument.text_data[0].value = water_depth.toFixed(1);
	instrument.text_data[1].value = "Water temp " + water_temperature.toFixed(1) + "ºC";

	instrument.two.update();
}

function instrument_depth_onclick(event)
{
	// check the generic instrument events
	instrument_onclick(me, event);
}

var me = {
        name : "Depth",
        init : instrument_depth_init,
        update : instrument_depth_update,
        onclick : instrument_depth_onclick,
};

mi_instrument_plugin.push(me);

})();

"use strict";

goog.require('proto.mi');
var msg_mi;
var pbmsg;

window.onload = main;
window.onresize = resize;

var mi_instrument_plugin = new Array();

var instrument = new Array();
var nrofinstruments = 0;

var ws;

var setup_done = 0;

var water_depth = 0.0;
var water_temperature = 0.0;
var water_current_direction = 0.0;
var water_current_speed = 0.0;

var wind_true_direction = 0.0;
var wind_true_speed = 0.0;

var boat_speed_through_water = 0.0;
var boat_speed_over_ground = 0.0;
var boat_course_over_ground = 0.0;
var boat_course_magnetic = 0.0;
var boat_heal = 0.0;

var ap_mode = 0;
var ap_course = 0.0;
var ap_speed = 0.0;

var service_bank_voltage = 0.0;
var service_bank_current = 0.0;

var fridge_temperature = 0.0;

var light_navigation = 0;
var light_engine = 0;
var light_anchor = 0;
var light_foredeck = 0;
var light_targadown = 0;
var light_targasearch = 0;
var light_boomdown = 0;
var light_cockpit = 0;

var simulator_mode = 1;

function lookup_instrument_plugin(name)
{
	var plugin = undefined;

	mi_instrument_plugin.forEach(p => {
		if (p.name == name) {
			plugin = p;
			return;
		}
	});
	return plugin;
}

function radian(degree)
{
	return (degree * Math.PI / 180.0);
}

function degree(radian)
{
	return (radian * 180.0 / Math.PI);
}

function makeTriangle(two, centerX, centerY, base, height)
{
          var tri = two.makePath(centerX-base/2, centerY+height/2, centerX, centerY-height/2,
		centerX+base/2, centerY+height/2);
          return tri;
}

function ws_init()
{
	ws = new WebSocket("ws://" + window.location.host + "/websocket", "mi");
	ws.binaryType = "arraybuffer";
	ws.onopen    = function(evt) { ws_onOpen(evt) };
	ws.onclose   = function(evt) { ws_onClose(evt) };
	ws.onmessage = function(evt) { ws_onMessage(evt) };
	ws.onerror   = function(evt) { ws_onError(evt) };
}

function ws_onOpen(evt)
{
	console.log("connected");
}

function ws_onClose(evt)
{
	console.log("disconnected - trying to reconnect...");
	setTimeout(function() {
		ws_init();
	}, 1000);
}

function ws_onMessage(evt)
{
	msg_mi = proto.mi.deserializeBinary(evt.data);

	if (msg_mi) {
		water_depth = msg_mi.getWaterDepth();
		water_temperature = msg_mi.getWaterTemperature();
		water_current_direction = msg_mi.getWaterCurrentDirection();
		water_current_speed = msg_mi.getWaterCurrentSpeed();

		wind_true_direction = msg_mi.getWindTrueDirection();
		wind_true_speed = msg_mi.getWindTrueSpeed();

		boat_speed_through_water = msg_mi.getBoatSpeedThroughWater();
		boat_speed_over_ground = msg_mi.getBoatSpeedOverGround();
		boat_course_over_ground = msg_mi.getBoatCourseOverGround();
		boat_course_magnetic = msg_mi.getBoatCourseMagnetic();
		boat_heal = msg_mi.getBoatHeal();

		ap_mode = msg_mi.getApMode();
		ap_course = msg_mi.getApCourse();

		service_bank_voltage = msg_mi.getServiceBankVoltage();
		service_bank_current = msg_mi.getServiceBankCurrent();

		fridge_temperature = msg_mi.getFridgeTemperature();

		light_navigation = msg_mi.getLightNavigation();
		light_engine = msg_mi.getLightEngine();
		light_anchor = msg_mi.getLightAnchor();
		light_foredeck = msg_mi.getLightForedeck();
		light_targadown = msg_mi.getLightTargadown();
		light_targasearch = msg_mi.getLightTargasearch();
		light_boomdown = msg_mi.getLightBoomdown();
		light_cockpit = msg_mi.getLightCockpit();

		simulator_mode = msg_mi.getSimulatorMode();

		instrument.forEach(i => {
			i.plugin.update(i);
		});
	}
}

function ws_onError(evt)
{
	console.log("error : " + evt.data);
}

function instrument_onclick(plugin, event)
{
	var area;

        area = plugin.instrument.button_prev.getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
                event.offsetY > area.top && event.offsetY < area.bottom) {
		return;
        }

        area = plugin.instrument.button_next.getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
                event.offsetY > area.top && event.offsetY < area.bottom) {
		return;
        }
}

function instrument_onmousedown(plugin, event)
{
}

function instrument_onmouseup(plugin, event)
{
}

function instrument_onmousemove(plugin, event)
{
}

function create_instrument(element)
{
	var i = nrofinstruments;

	instrument.push({},);
	instrument[i].element = element;
	instrument[i].default = element.getAttribute("default");
	instrument[i].plugin = lookup_instrument_plugin(instrument[i].default);
	if (instrument[i].plugin == undefined) {
		console.log("There is no instrument named " + instrument[i].default);
		instrument.pop();
		return;
	}
	instrument[i].plugin.instrument = instrument[i];
	instrument[i].width = instrument[i].element.clientWidth;
	instrument[i].height = instrument[i].element.clientHeight;
	instrument[i].size = Math.min(instrument[i].width, instrument[i].height);
	instrument[i].two = new Two({
		type: Two.Types.canvas,
		width: instrument[i].width,
		height: instrument[i].height,
	}).appendTo(instrument[i].element);
	instrument[i].canvas = element.getElementsByTagName("canvas")[0];
	instrument[i].canvas.onclick = instrument[i].plugin.onclick;
	instrument[i].canvas.onmousedown = instrument[i].plugin.onmousedown;
	instrument[i].canvas.onmouseup = instrument[i].plugin.onmouseup;
	instrument[i].canvas.onmousemove = instrument[i].plugin.onmousemove;

	instrument[i].frame = instrument[i].two.makeRoundedRectangle(
		instrument[i].width/2., instrument[i].height/2,
		instrument[i].width*0.98, instrument[i].height*0.98,
		instrument[i].size*0.02);
	instrument[i].frame.stroke = "black";
	instrument[i].frame.linewidth = instrument[i].size*0.02;

	instrument[i].button_prev = makeTriangle(instrument[i].two,
		instrument[i].width*0.1, instrument[i].height*0.9,
		instrument[i].size*0.1, instrument[i].size*0.1);
	instrument[i].button_prev.fill = "black";
	instrument[i].button_prev.rotation = radian(-90);

	instrument[i].instrument_name = instrument[i].two.makeText(instrument[i].plugin.name,
		instrument[i].width*0.5, instrument[i].height*0.9);
	instrument[i].instrument_name.size = instrument[i].size*0.1;
	instrument[i].instrument_name.fill = "black";

	instrument[i].button_next = makeTriangle(instrument[i].two,
		instrument[i].width*0.9, instrument[i].height*0.9,
		instrument[i].size*0.1, instrument[i].size*0.1);
	instrument[i].button_next.fill = "black";
	instrument[i].button_next.rotation = radian(90);

	instrument[i].plugin.init(instrument[i]);
	instrument[i].plugin.update(instrument[i]);

	nrofinstruments++;
}

function resize()
{
	instrument.forEach(i => {
		i.plugin.update(i);
	});
}

function main()
{
	var instruments = document.getElementsByClassName("mi-instrument");
	for (var i=0; i<instruments.length; i++) {
		create_instrument(instruments[i]);
	}

	ws_init();
}

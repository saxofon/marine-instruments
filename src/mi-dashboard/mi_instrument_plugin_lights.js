"use strict";

(function(){ 

function instrument_light_init(instrument)
{
	instrument.light = new Array();

	// navlight
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.26, instrument.height*0.1,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[0].fill = "silver";
	instrument.light.push(instrument.two.makeText("Navigation", instrument.width*0.26, instrument.height*0.1));
	instrument.light[1].size = instrument.size * 0.08;
	instrument.light[1].fill = "black";

	// engine light
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.26, instrument.height*0.25,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[2].fill = "silver";
	instrument.light.push(instrument.two.makeText("Engine", instrument.width*0.26, instrument.height*0.25));
	instrument.light[3].size = instrument.size * 0.08;
	instrument.light[3].fill = "black";

	// anchor
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.26, instrument.height*0.40,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[4].fill = "silver";
	instrument.light.push(instrument.two.makeText("Anchor", instrument.width*0.26, instrument.height*0.40));
	instrument.light[5].size = instrument.size * 0.08;
	instrument.light[5].fill = "black";

	// foredeck flood light
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.26, instrument.height*0.55,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[6].fill = "silver";
	instrument.light.push(instrument.two.makeText("Foredeck", instrument.width*0.26, instrument.height*0.55));
	instrument.light[7].size = instrument.size * 0.08;
	instrument.light[7].fill = "black";

	// targa down light
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.74, instrument.height*0.1,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[8].fill = "silver";
	instrument.light.push(instrument.two.makeText("Targa down", instrument.width*0.74, instrument.height*0.1));
	instrument.light[9].size = instrument.size * 0.08;
	instrument.light[9].fill = "black";

	// targa search light
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.74, instrument.height*0.25,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[10].fill = "silver";
	instrument.light.push(instrument.two.makeText("Targa search", instrument.width*0.74, instrument.height*0.25));
	instrument.light[11].size = instrument.size * 0.08;
	instrument.light[11].fill = "black";

	// boom downlight
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.74, instrument.height*0.40,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[12].fill = "silver";
	instrument.light.push(instrument.two.makeText("Boom down", instrument.width*0.74, instrument.height*0.40));
	instrument.light[13].size = instrument.size * 0.08;
	instrument.light[13].fill = "black";

	// cockpit tent light
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.74, instrument.height*0.55,
		instrument.width*0.47, instrument.height*0.12));
	instrument.light[14].fill = "silver";
	instrument.light.push(instrument.two.makeText("Cockpit", instrument.width*0.74, instrument.height*0.55));
	instrument.light[15].size = instrument.size * 0.08;
	instrument.light[15].fill = "black";

	// Instrument light dim control
	instrument.light.push(instrument.two.makeRoundedRectangle(instrument.width*0.5, instrument.height*0.75,
		instrument.width*0.95, instrument.height*0.12));
	instrument.light[16].fill = "silver";
	instrument.light.push(instrument.two.makeText("-", instrument.width*0.15, instrument.height*0.75));
	instrument.light[17].size = instrument.size * 0.2;
	instrument.light[17].fill = "black";
	instrument.light.push(instrument.two.makeText("Instruments", instrument.width*0.5, instrument.height*0.75));
	instrument.light[18].size = instrument.size * 0.08;
	instrument.light[18].fill = "black";
	instrument.light.push(instrument.two.makeText("+", instrument.width*0.85, instrument.height*0.75));
	instrument.light[19].size = instrument.size * 0.2;
	instrument.light[19].fill = "black";
}

function instrument_light_update(instrument)
{
	if (light_navigation == 0) {
		instrument.light[0].fill = "silver";
	} else {
		instrument.light[0].fill = "yellow";
	}

	if (light_engine == 0) {
		instrument.light[2].fill = "silver";
	} else {
		instrument.light[2].fill = "yellow";
	}

	if (light_anchor == 0) {
		instrument.light[4].fill = "silver";
	} else {
		instrument.light[4].fill = "yellow";
	}

	if (light_foredeck == 0) {
		instrument.light[6].fill = "silver";
	} else {
		instrument.light[6].fill = "yellow";
	}

	if (light_targadown == 0) {
		instrument.light[8].fill = "silver";
	} else {
		instrument.light[8].fill = "yellow";
	}

	if (light_targasearch == 0) {
		instrument.light[10].fill = "silver";
	} else {
		instrument.light[10].fill = "yellow";
	}

	if (light_boomdown == 0) {
		instrument.light[12].fill = "silver";
	} else {
		instrument.light[12].fill = "yellow";
	}

	if (light_cockpit == 0) {
		instrument.light[14].fill = "silver";
	} else {
		instrument.light[14].fill = "yellow";
	}

	instrument.two.update();
}

function instrument_light_onclick(event)
{
	var area;

	// check the generic instrument events
	instrument_onclick(me, event);

        area = me.instrument.light[0].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_navigation)
                        msg_mi.setLightNavigation(0);
                else
                        msg_mi.setLightNavigation(1);
        }

        area = me.instrument.light[2].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_engine)
                        msg_mi.setLightEngine(0);
                else
                        msg_mi.setLightEngine(1);
        }

        area = me.instrument.light[4].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_anchor)
                        msg_mi.setLightAnchor(0);
                else
                        msg_mi.setLightAnchor(1);
        }

        area = me.instrument.light[6].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_foredeck)
                        msg_mi.setLightForedeck(0);
                else
                        msg_mi.setLightForedeck(1);
        }

        area = me.instrument.light[8].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_targadown)
                        msg_mi.setLightTargadown(0);
                else
                        msg_mi.setLightTargadown(1);
        }

        area = me.instrument.light[10].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_targasearch)
                        msg_mi.setLightTargasearch(0);
                else
                        msg_mi.setLightTargasearch(1);
        }

        area = me.instrument.light[12].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_boomdown)
                        msg_mi.setLightBoomdown(0);
                else
                        msg_mi.setLightBoomdown(1);
        }

        area = me.instrument.light[14].getBoundingClientRect();
        if (event.offsetX > area.left && event.offsetX < area.right &&
            event.offsetY > area.top && event.offsetY < area.bottom) {
                if (light_cockpit)
                        msg_mi.setLightCockpit(0);
                else
                        msg_mi.setLightCockpit(1);
        }

	pbmsg = msg_mi.serializeBinary();
	ws.send(pbmsg);
}

function instrument_light_onmousedown(event)
{
}

function instrument_light_onmouseup(event)
{
}

function instrument_light_onmousemove(event)
{
}

var me = {
        name : "Lights",
        init : instrument_light_init,
        update : instrument_light_update,
        onclick : instrument_light_onclick,
        onmousedown : instrument_light_onmousedown,
        onmouseup : instrument_light_onmouseup,
        onmousemove : instrument_light_onmousemove,
};

mi_instrument_plugin.push(me);

})(); 

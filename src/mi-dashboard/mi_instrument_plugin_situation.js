"use strict";

(function(){ 

var dragging = false;

function instrument_situation_init(instrument)
{
	var i = 0;
	instrument.dot = new Array();

	// compass
	instrument.compassring = instrument.two.makeArcSegment(
		instrument.size/2, instrument.size/2,
		instrument.size/2*0.6, instrument.size/2*0.8,
		radian(0.0), radian(360.0));
	instrument.compassring.fill = "black";
	instrument.compassring.stroke = "black";
	instrument.compassring.linewidth = 1;
	for (var g=0; g<360; g+=30) {
		if (g==0) {
			instrument.text_n = instrument.two.makeText("N",
				instrument.size/2 + Math.sin(radian(g))*instrument.size/2*0.7,
				instrument.size/2 - Math.cos(radian(g))*instrument.size/2*0.7);
			instrument.text_n.fill = "white";
			instrument.text_n.size = instrument.size*0.08;
		} else if (g==90) {
			instrument.text_e = instrument.two.makeText("E",
				instrument.size/2 + Math.sin(radian(g))*instrument.size/2*0.7,
				instrument.size/2 - Math.cos(radian(g))*instrument.size/2*0.7);
			instrument.text_e.fill = "white";
			instrument.text_e.size = instrument.size*0.08;
		} else if (g==180) {
			instrument.text_s = instrument.two.makeText("S",
				instrument.size/2 + Math.sin(radian(g))*instrument.size/2*0.7,
				instrument.size/2 - Math.cos(radian(g))*instrument.size/2*0.7);
			instrument.text_s.fill = "white";
			instrument.text_s.size = instrument.size*0.08;
		} else if (g==270) {
			instrument.text_w = instrument.two.makeText("W",
				instrument.size/2 + Math.sin(radian(g))*instrument.size/2*0.7,
				instrument.size/2 - Math.cos(radian(g))*instrument.size/2*0.7);
			instrument.text_w.fill = "white";
			instrument.text_w.size = instrument.size*0.08;
		} else {
			instrument.dot[i] = instrument.two.makeCircle(
				instrument.size/2 + Math.sin(radian(g))*instrument.size/2*0.7,
				instrument.size/2 - Math.cos(radian(g))*instrument.size/2*0.7,
				instrument.size*0.01);
			instrument.dot[i].fill = "white";
			instrument.dot[i].noStroke();
			i++;
		}
		
	}
	instrument.compass = instrument.two.makeGroup(
		instrument.compassring, instrument.text_n, instrument.text_e,
		instrument.text_s, instrument.text_w, instrument.dot[0],
		instrument.dot[1], instrument.dot[2], instrument.dot[3],
		instrument.dot[4], instrument.dot[5], instrument.dot[6],
		instrument.dot[7]);
	instrument.compass.center();

	// autopilot commanded course
	instrument.apmark = makeTriangle(instrument.two, instrument.size/2, instrument.size/2,
		instrument.size*0.1, instrument.size*0.1);
	instrument.apmark.fill = "red";
	instrument.apbox = instrument.two.makeRoundedRectangle(instrument.width*0.85, instrument.height*0.1, instrument.width*0.2, instrument.size*0.1);
	instrument.apbox.fill = "red";
	instrument.aptxt = instrument.two.makeText("", instrument.width*0.85, instrument.height*0.1);
	instrument.aptxt.fill = "black";
	instrument.aptxt.size = instrument.size*0.05;

	// boat course
	instrument.bcbox = instrument.two.makeRoundedRectangle(instrument.size/2, instrument.size*0.15,
		instrument.size*0.15, instrument.size*0.13);
	instrument.bcbox.fill = "white";
	instrument.bctxt = instrument.two.makeText("", instrument.size/2, instrument.size*0.15);
	instrument.bctxt.fill = "black";
	instrument.bctxt.size = instrument.size*0.08;

	// heal indicator
/* ?????? instrument.two.makeArcSegment seems broken...
	var sa = 180.0-15.0;
	var ea = 180.0+15.0;
	instrument.healbox = instrument.two.makeArcSegment(
		instrument.size/2, instrument.size/2,
		instrument.size/2*0.57, instrument.size/2*0.83,
		radian(sa), radian(ea));
*/
	instrument.healbox = instrument.two.makeRoundedRectangle(instrument.size/2, instrument.size/2,
		instrument.size*0.1, instrument.size*0.07);
	instrument.healbox.fill = "antiquewhite";
	instrument.healbox.stroke = "black";
	instrument.healbox.linewidth = 1;
	instrument.healtxt = instrument.two.makeText("", instrument.size/2, instrument.size/2);
	instrument.healtxt.fill = "black";
	instrument.healtxt.size = instrument.size*0.05;
	instrument.heal = instrument.two.makeGroup(instrument.healbox, instrument.healtxt);
	instrument.heal.center();

	instrument.wcbox = makeTriangle(instrument.two, instrument.size/2, instrument.size/2,
		instrument.size*0.1, instrument.size*0.3);
	instrument.wcbox.fill = "aquamarine";
	instrument.wctxt = instrument.two.makeText("", instrument.size/2, instrument.size/2*1.2);
	instrument.wctxt.fill = "black";
	instrument.wctxt.size = instrument.size*0.05;
	instrument.wc = instrument.two.makeGroup(instrument.wcbox, instrument.wctxt);
	instrument.wc.center();

	instrument.windbox = instrument.two.makeRoundedRectangle(instrument.size/2, instrument.size/2,
		instrument.size*0.1, instrument.size*0.07);
	instrument.windbox.fill = "lightskyblue";
	instrument.windbox.stroke = "black";
	instrument.windbox.linewidth = 1;
	instrument.windtxt = instrument.two.makeText("0", instrument.size/2, instrument.size/2);
	instrument.windtxt.fill = "black";
	instrument.windtxt.size = instrument.size*0.05;
	instrument.wind = instrument.two.makeGroup(instrument.windbox, instrument.windtxt);
	instrument.wind.center();
}

function instrument_situation_update(instrument)
{
	// compass ring
	instrument.compass.rotation = radian(-boat_course_magnetic);
	instrument.compass.translation.set(instrument.size/2, instrument.size/2);

	// boat course
	instrument.bctxt.value = boat_course_magnetic.toFixed(0);

	// heal indicator
	instrument.heal.rotation = radian(-boat_heal);
	instrument.heal.translation.set(instrument.size/2 - Math.sin(radian(boat_heal+180.0))*instrument.size/2*0.6,
		 instrument.size/2 - Math.cos(radian(boat_heal+180.0))*instrument.size/2*0.6);
	instrument.healtxt.value = Math.abs(boat_heal.toFixed(0));

	// autopilot
	instrument.apmark.rotation = radian(-boat_course_magnetic+ap_course);
	instrument.apmark.translation.set(instrument.size/2 - Math.sin(radian(boat_course_magnetic-ap_course))*instrument.size/2*0.5,
		 instrument.size/2 - Math.cos(radian(boat_course_magnetic-ap_course))*instrument.size/2*0.5);
	instrument.aptxt.value = "AP: " + ap_course.toFixed(0);
	if (ap_mode) {
		instrument.apmark.fill = "green";
		instrument.apbox.fill = "green";
	} else {
		instrument.apmark.fill = "red";
		instrument.apbox.fill = "red";
	}

	// water current indicator
	instrument.wc.rotation = radian(-boat_course_over_ground+water_current_direction);
	instrument.wc.translation.set(instrument.size/2, instrument.size/2);
	instrument.wctxt.value = water_current_speed.toFixed(1);
	var diff = Math.abs(-boat_course_over_ground+water_current_direction);
	if (diff > 90.0) {
		instrument.wctxt.rotation = radian(180.0);
	} else {
		instrument.wctxt.rotation = radian(0.0);
	}

	// wind indicator
	instrument.wind.rotation = radian(-boat_course_over_ground+wind_true_direction);
	instrument.wind.translation.set(instrument.size/2 + Math.sin(radian(-boat_course_over_ground+wind_true_direction))*instrument.size/2*0.8,
		 instrument.size/2 - Math.cos(radian(-boat_course_over_ground+wind_true_direction))*instrument.size/2*0.8);
	instrument.windtxt.value = wind_true_speed.toFixed(1);
	if (Math.cos(radian(-boat_course_over_ground+wind_true_direction)) < 0.0) {
		instrument.windtxt.rotation = radian(180.0);
	} else {
		instrument.windtxt.rotation = radian(0.0);
	}
	

	instrument.two.update();
}

function instrument_situation_onclick(event)
{
	// check the generic instrument events
	instrument_onclick(me, event);

	// autopilot events
	var area_ap = me.instrument.apbox.getBoundingClientRect();
	if (event.offsetX > area_ap.left && event.offsetX < area_ap.right &&
	    event.offsetY > area_ap.top && event.offsetY < area_ap.bottom) {
		if (ap_mode)
			msg_mi.setApMode(0);
		else
			msg_mi.setApMode(1);
		msg_mi.setApCourse(ap_course);
		pbmsg = msg_mi.serializeBinary();
		ws.send(pbmsg);
		return;
	}
}

function instrument_situation_onmousedown(event)
{
	dragging = true;
}

function instrument_situation_onmouseup(event)
{
	dragging = false;
}

function instrument_situation_onmousemove(event)
{
	if (!dragging)
		return;
	var radians = Math.atan2(event.offsetX - me.instrument.size/2,
		me.instrument.size/2 - event.offsetY);
	var degrees = degree(radians) + boat_course_magnetic;
	if (degrees>360.0) degrees -= 360.0;
	if (degrees<0.0) degrees += 360.0;
	if (degree != ap_course) {
		msg_mi.setApCourse(degrees);
		pbmsg = msg_mi.serializeBinary();
		ws.send(pbmsg);
		return;
	}
}

var me = {
	name : "Situation",
	init : instrument_situation_init,
	update : instrument_situation_update,
	onclick : instrument_situation_onclick,
	onmousedown : instrument_situation_onmousedown,
	onmouseup : instrument_situation_onmouseup,
	onmousemove : instrument_situation_onmousemove,
};

mi_instrument_plugin.push(me);

})(); 

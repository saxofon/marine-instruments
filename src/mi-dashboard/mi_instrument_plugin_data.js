"use strict";

(function(){ 

function instrument_data_init(instrument)
{
	instrument.text_data = new Array();
	instrument.text_data.push(instrument.two.makeText("", instrument.width*0.5, instrument.height*3/20));
	instrument.text_data[0].size = instrument.size*0.2;
	instrument.text_data[0].fill = "black";

	instrument.text_data.push(instrument.two.makeText("", instrument.width*0.5, instrument.height*6/20));
	instrument.text_data[1].size = instrument.size*0.2;
	instrument.text_data[1].fill = "black";

	instrument.text_data.push(instrument.two.makeText("", instrument.width*0.5, instrument.height*13/20));
	instrument.text_data[2].size = instrument.size*0.1;
	instrument.text_data[2].fill = "black";

	instrument.text_data.push(instrument.two.makeText("", instrument.width*0.5, instrument.height*15/20));
	instrument.text_data[3].size = instrument.size*0.1;
	instrument.text_data[3].fill = "black";
}

function instrument_data_update(instrument)
{
	instrument.text_data[0].value = "STW  " + boat_speed_through_water.toFixed(1);
	instrument.text_data[1].value = "SOG  " + boat_speed_over_ground.toFixed(1);
	instrument.text_data[2].value = "Battery " + service_bank_voltage.toFixed(1) + "V/" + service_bank_current.toFixed(1) + "A";
	instrument.text_data[3].value = "Fridge temp " + fridge_temperature.toFixed(1) + "ºC";

	instrument.two.update();
}

function instrument_data_onclick(event)
{
	// check the generic instrument events
	instrument_onclick(me, event);
}

var me = {
        name : "Data",
        init : instrument_data_init,
        update : instrument_data_update,
        onclick : instrument_data_onclick,
};

mi_instrument_plugin.push(me);

})(); 

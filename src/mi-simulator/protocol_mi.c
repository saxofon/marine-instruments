#include <string.h>

#if !defined (LWS_PLUGIN_STATIC)
#define LWS_DLL
#define LWS_INTERNAL
#include <libwebsockets.h>
#endif

#include <mi.pb-c.h>

struct msg {
	void *payload; /* is malloc'd */
	size_t len;
};

struct per_session_data_mi {
	struct per_session_data_mi *pss_list;
	struct lws *wsi;
};

struct per_vhost_data_mi {
	struct lws_context *context;
	struct lws_vhost *vhost;
	const struct lws_protocols *protocol;

	struct per_session_data_mi *pss_list; /* linked-list of live pss*/
};

static void _mi_destroy_message(void *_msg)
{
	struct msg *msg = _msg;

	free(msg->payload);
	msg->payload = NULL;
	msg->len = 0;
}

static int callback_mi(struct lws *wsi, enum lws_callback_reasons reason,
	void *user, void *in, size_t len)
{
	struct per_session_data_mi *pss = (struct per_session_data_mi *)user;
	struct per_vhost_data_mi *vhd = (struct per_vhost_data_mi *)
			lws_protocol_vh_priv_get(lws_get_vhost(wsi), lws_get_protocol(wsi));

	switch (reason) {
	case LWS_CALLBACK_PROTOCOL_INIT:
		vhd = lws_protocol_vh_priv_zalloc(lws_get_vhost(wsi), lws_get_protocol(wsi),
				sizeof(struct per_vhost_data_mi));
		vhd->context = lws_get_context(wsi);
		vhd->protocol = lws_get_protocol(wsi);
		vhd->vhost = lws_get_vhost(wsi);
		break;

	case LWS_CALLBACK_PROTOCOL_DESTROY:
		break;

	case LWS_CALLBACK_WSI_CREATE:
		lwsl_notice("LWS_CALLBACK_WSI_CREATE:\n");
		break;

	case LWS_CALLBACK_WSI_DESTROY:
		lwsl_notice("LWS_CALLBACK_WSI_CREATE:\n");
		break;

	case LWS_CALLBACK_ESTABLISHED:
		lws_ll_fwd_insert(pss, pss_list, vhd->pss_list);
		pss->wsi = wsi;
		break;

	case LWS_CALLBACK_CLOSED:
		/* remove our closing pss from the list of live pss */
		lws_ll_fwd_remove(struct per_session_data_mi, pss_list,
				  pss, vhd->pss_list);
		break;

	case LWS_CALLBACK_SERVER_WRITEABLE: {
		int status;
		struct msg wsmsg;
		Mi pbmsg = MI__INIT;

//printf("packing and sending\n");

		pbmsg.version = 0.1;
		pbmsg.water_depth = water_depth;
		pbmsg.water_temperature = water_temperature;
		pbmsg.water_current_direction = water_current_direction;
		pbmsg.water_current_speed = water_current_speed;
		pbmsg.wind_true_direction = wind_true_direction;
		pbmsg.wind_true_speed = wind_true_speed;
		pbmsg.boat_speed_through_water = boat_speed_through_water;
		pbmsg.boat_speed_over_ground = boat_speed_over_ground;
		pbmsg.boat_course_over_ground = boat_course_over_ground;
		pbmsg.boat_course_magnetic = boat_course_magnetic;
		pbmsg.boat_heal = boat_heal;
		pbmsg.ap_mode = ap_mode;
		pbmsg.ap_course = ap_course;
		pbmsg.service_bank_voltage = service_bank_voltage;
		pbmsg.service_bank_current = service_bank_current;
		pbmsg.fridge_temperature = fridge_temperature;
		pbmsg.light_navigation = light_navigation;
		pbmsg.light_engine = light_engine;
		pbmsg.light_anchor = light_anchor;
		pbmsg.light_foredeck = light_foredeck;
		pbmsg.light_targadown = light_targadown;
		pbmsg.light_targasearch = light_targasearch;
		pbmsg.light_boomdown = light_boomdown;
		pbmsg.light_cockpit = light_cockpit;
		pbmsg.simulator_mode = simulator_mode;

		wsmsg.len = mi__get_packed_size(&pbmsg);
		wsmsg.payload = malloc(LWS_PRE + wsmsg.len);
		if (!wsmsg.payload) {
			lwsl_err("OOM: tx canceled\n");
			break;
		}

		mi__pack(&pbmsg, (uint8_t *)wsmsg.payload + LWS_PRE);

		status = lws_write(wsi, ((unsigned char *)wsmsg.payload) +
			      LWS_PRE, wsmsg.len, LWS_WRITE_BINARY);
		if (status < (int)wsmsg.len) {
			lwsl_err("ERROR %d writing to ws socket\n", status);
			return -1;
		}

		break;
	}

	case LWS_CALLBACK_RECEIVE: {
		Mi *mi;
		int s;

//printf("unpacking\n");

		mi = mi__unpack(NULL, len, in);

		ap_mode = mi->ap_mode;
		ap_course = mi->ap_course;

		light_navigation = mi->light_navigation;
		light_engine = mi->light_engine;
		light_anchor = mi->light_anchor;
		light_foredeck = mi->light_foredeck;
		light_targadown = mi->light_targadown;
		light_targasearch = mi->light_targasearch;
		light_boomdown = mi->light_boomdown;
		light_cockpit = mi->light_cockpit;

		simulator_mode = mi->simulator_mode;

		mi__free_unpacked(mi, NULL);

		break;
	}

	default:
		break;
	}

	return 0;
}

#define PROTOCOL_MI \
	{ \
		"mi", \
		callback_mi, \
		sizeof(struct per_session_data_mi), \
		0, \
		0, NULL, 0 \
	}

#if !defined (LWS_PLUGIN_STATIC)

static const struct lws_protocols protocols[] = {
	PROTOCOL_MI
};

LWS_EXTERN LWS_VISIBLE int init_protocol_mi(struct lws_context *context,
	struct lws_plugin_capability *c)
{
	if (c->api_magic != LWS_PLUGIN_API_MAGIC) {
		lwsl_err("Plugin API %d, library API %d", LWS_PLUGIN_API_MAGIC,
			 c->api_magic);
		return 1;
	}

	c->protocols = protocols;
	c->count_protocols = LWS_ARRAY_SIZE(protocols);
	c->extensions = NULL;
	c->count_extensions = 0;

	return 0;
}

LWS_EXTERN LWS_VISIBLE int
destroy_protocol_mi(struct lws_context *context)
{
	return 0;
}
#endif

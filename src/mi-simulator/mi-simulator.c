#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>
#include <math.h>
#include <pthread.h>
#include <sys/param.h>
#include <libwebsockets.h>

static float water_depth = 0.0;
static float water_temperature = 0.0;
static float water_current_direction = 0.0;
static float water_current_speed = 0.0;

static float wind_true_direction = 0.0;
static float wind_true_direction_5_min = 0.0;
static float wind_true_direction_5_max = 0.0;
static float wind_true_direction_15_min = 0.0;
static float wind_true_direction_15_max = 0.0;
static float wind_true_direction_60_min = 0.0;
static float wind_true_direction_60_max = 0.0;

static float wind_true_speed = 0.0;
static float wind_true_speed_5_min = 0.0;
static float wind_true_speed_5_max = 0.0;
static float wind_true_speed_15_min = 0.0;
static float wind_true_speed_15_max = 0.0;
static float wind_true_speed_60_min = 0.0;
static float wind_true_speed_60_max = 0.0;

static float boat_speed_through_water = 0.0;
static float boat_speed_over_ground = 0.0;
static float boat_course_over_ground = 0.0;
static float boat_course_magnetic = 0.0;
static float boat_heal = 0.0;

static unsigned int ap_mode = 0;
static float ap_course = 0.0;

static float service_bank_voltage = 0.0;
static float service_bank_current = 0.0;

static float fridge_temperature = 0.0;

static unsigned int light_navigation = 0.0;
static unsigned int light_engine = 0.0;
static unsigned int light_anchor = 0.0;
static unsigned int light_foredeck = 0.0;
static unsigned int light_targadown = 0.0;
static unsigned int light_targasearch = 0.0;
static unsigned int light_boomdown = 0.0;
static unsigned int light_cockpit = 0.0;

static unsigned int simulator_mode = 1;

#define LWS_PLUGIN_STATIC
#include "protocol_mi.c"

static struct lws_protocols protocols[] = {
	{ "http", lws_callback_http_dummy, 0, 0 },
	PROTOCOL_MI,
	{ NULL, NULL, 0, 0 }
};

static int interrupted;

static const struct lws_http_mount mount = {
	/* .mount_next */		NULL,		/* linked-list "next" */
	/* .mountpoint */		"/",		/* mountpoint URL */
	/* .origin */			"./mount-origin",  /* serve from dir */
	/* .def */			"index.html",	/* default filename */
	/* .protocol */			NULL,
	/* .cgienv */			NULL,
	/* .extra_mimetypes */		NULL,
	/* .interpret */		NULL,
	/* .cgi_timeout */		0,
	/* .cache_max_age */		0,
	/* .auth_mask */		0,
	/* .cache_reusable */		0,
	/* .cache_revalidate */		0,
	/* .cache_intermediaries */	0,
	/* .origin_protocol */		LWSMPRO_FILE,	/* files in a dir */
	/* .mountpoint_len */		1,		/* char count */
	/* .basic_auth_login_file */	NULL,
};

void sigint_handler(int sig)
{
	interrupted = 1;
}

float degree(float radian)
{
    return radian * 180.0 / M_PI;
}

float radian(float degree)
{
    return degree * M_PI / 180.0;
}

#define NSEC_PER_SEC   1000000000UL
#define NSEC_PER_MSEC     1000000UL
#define NSEC_PER_USEC        1000UL
#define USEC_PER_SEC      1000000UL
#define USEC_PER_MSEC        1000UL
#define MSEC_PER_SEC         1000UL

static void ts_normalize(struct timespec *t)
{
        while (t->tv_nsec >= NSEC_PER_SEC) {
                t->tv_sec++;
                t->tv_nsec -= NSEC_PER_SEC;
        }
	if ((t->tv_sec >= 1) && (t->tv_nsec < 0)) {
                t->tv_sec--;
                t->tv_nsec += NSEC_PER_SEC;
	}
}

static struct timespec ts_add(struct timespec t1, struct timespec t2)
{
        struct timespec result;

        result.tv_sec = t1.tv_sec + t2.tv_sec;
        result.tv_nsec = t1.tv_nsec + t2.tv_nsec;

	ts_normalize(&result);

        return (result);
}

static void *thread_simulator(void *arg)
{
	struct timespec mark, period;
	struct sched_param threadsched;
	float diff;

	period.tv_sec = 0;
	period.tv_nsec = 350 * NSEC_PER_MSEC;
	ts_normalize(&period);

	clock_gettime(CLOCK_MONOTONIC, &mark);
	for (;;) {
		mark = ts_add(mark, period);

		if (simulator_mode) {

//printf("updating simulator-frame\n");

			// water conditions simulator
			water_depth = 10.0 + sinf(radian(mark.tv_sec % 360)) * 5.0;
			water_temperature = 20.0 + cosf(radian(mark.tv_sec % 360)) * 2.0;
			water_current_direction = 90.0 + sinf(radian(mark.tv_sec % 360)) * 40.0;
			water_current_speed = 1.0 + sinf(radian(mark.tv_sec % 360));
			water_current_direction = 90.0;

			// boat speed simulator
			diff = fabsf(boat_course_over_ground - wind_true_direction);
			if (diff < 30) {
				if (boat_speed_through_water > 0.0) {
					boat_speed_through_water -= 0.1;
					if (boat_speed_through_water < 0.4)
						boat_speed_through_water = 0.0;
				}
			} else {
				if (boat_speed_through_water < 8.5) {
					boat_speed_through_water += 0.5;
					if (boat_speed_through_water > 8.6)
						boat_speed_through_water = 8.5;
				}
			}
			boat_speed_over_ground = boat_speed_through_water +
				water_current_speed * cosf(radian(boat_course_over_ground - water_current_direction));

			// wind simulator
			wind_true_direction = 0.0 + sinf(radian(mark.tv_sec % 360)) * 20.0;
			wind_true_speed = 8.0 + sinf(radian(mark.tv_sec % 30)) * 8.0 *
				(float)rand()/(float)(RAND_MAX);

			// boat heal simulator
			diff = boat_course_over_ground - wind_true_direction;
			if (diff < 0.0)
				diff += 360.0;
			if (diff > 360.0)
				diff -= 360.0;
			boat_heal = 30.0 * sinf(radian(diff));

		// autopilot simulator for compass holding
		if (ap_mode == 1) {
			if (ap_course != boat_course_magnetic) {
				float starboard_diff = ap_course - boat_course_magnetic;
				float port_diff = boat_course_magnetic - ap_course;
				if (starboard_diff > 360.0) starboard_diff -= 360.0;
				if (starboard_diff < 0.0) starboard_diff += 360.0;
				if (port_diff > 360.0) port_diff -= 360.0;
				if (port_diff < 0.0) port_diff += 360.0;
				if (starboard_diff < port_diff) {
					boat_course_magnetic += MAX(starboard_diff/18, 1.0);
					if (starboard_diff < 1.5) {
						boat_course_magnetic = ap_course;
					}
				} else {
					boat_course_magnetic -= MAX(port_diff/18, 1.0);
					boat_course_magnetic -= port_diff/18;
					if (port_diff < 1.5) {
						boat_course_magnetic = ap_course;
					}
				}
			}
			if (boat_course_magnetic > 360.0)
				boat_course_magnetic -= 360.0;
			if (boat_course_magnetic < 0.0)
				boat_course_magnetic += 360.0;
		}
			boat_course_over_ground = boat_course_magnetic;

			service_bank_voltage = 13.2;
			service_bank_current = 1.0 + sinf(radian(mark.tv_sec % 360)) * 3.0;

			fridge_temperature = 4.5;

		}
		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &mark, NULL);
	}
}

static void *thread_data_exporter(void *arg)
{
	struct lws_context *context = (struct lws_context *)arg;
	struct timespec mark, period;
	struct sched_param threadsched;

	period.tv_sec = 0;
	period.tv_nsec = 350 * NSEC_PER_MSEC;
	ts_normalize(&period);

	clock_gettime(CLOCK_MONOTONIC, &mark);
	for (;;) {
		mark = ts_add(mark, period);

//printf("trigger pack/send\n");

		lws_callback_on_writable_all_protocol(context, &protocols[1]);
		lws_cancel_service(context);

		clock_nanosleep(CLOCK_MONOTONIC, TIMER_ABSTIME, &mark, NULL);
	}
}

int main(int argc, const char **argv)
{
	struct lws_context_creation_info info;
	struct lws_context *context;
	const char *p;
	int n = 0, logs = LLL_USER | LLL_ERR | LLL_WARN | LLL_NOTICE
			/* for LLL_ verbosity above NOTICE to be built into lws,
			 * lws must have been configured and built with
			 * -DCMAKE_BUILD_TYPE=DEBUG instead of =RELEASE */
			/* | LLL_INFO */ /* | LLL_PARSER */ /* | LLL_HEADER */
			/* | LLL_EXT */ /* | LLL_CLIENT */ /* | LLL_LATENCY */
			/* | LLL_DEBUG */;
	pthread_t tid_simulator;
	pthread_t tid_data_exporter;
	int status;

	signal(SIGINT, sigint_handler);

	if ((p = lws_cmdline_option(argc, argv, "-d")))
		logs = atoi(p);

	lws_set_log_level(logs, NULL);

	memset(&info, 0, sizeof info);
	info.port = 8444;
	info.mounts = &mount;
	info.protocols = protocols;
	info.vhost_name = "0.0.0.0";
	info.options = LWS_SERVER_OPTION_HTTP_HEADERS_SECURITY_BEST_PRACTICES_ENFORCE;

	if (lws_cmdline_option(argc, argv, "-s")) {
		lwsl_user("Server using TLS\n");
		info.options |= LWS_SERVER_OPTION_DO_SSL_GLOBAL_INIT;
		info.ssl_cert_filepath = "localhost-100y.cert";
		info.ssl_private_key_filepath = "localhost-100y.key";
	}

	if (lws_cmdline_option(argc, argv, "-h"))
		info.options |= LWS_SERVER_OPTION_VHOST_UPG_STRICT_HOST_CHECK;

	context = lws_create_context(&info);
	if (!context) {
		lwsl_err("lws init failed\n");
		return 1;
	}

	status = pthread_create(&tid_simulator, NULL, thread_simulator, NULL);
	status = pthread_create(&tid_data_exporter, NULL, thread_data_exporter, (void*)context);

	while (n >= 0 && !interrupted) {
		n = lws_service(context, 1000);
	}

	lws_context_destroy(context);

	return 0;
}

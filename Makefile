TOP := $(PWD)
BD := $(TOP)/build

REPOS += $(BD)/two.js
REPO_TWOJS_URL = https://github.com/jonobr1/two.js.git
REPO_TWOJS_MIRROR = /home/per/repositories/two.js.git
REPO_TWOJS_VER = v0.6.0

REPOS += $(BD)/libwebsockets
REPO_LWS_URL = https://libwebsockets.org/repo/libwebsockets
REPO_LWS_MIRROR = /home/per/repositories/libwebsockets.git
REPO_LWS_VER = v4.1.6

REPOS += $(BD)/closure-library
REPO_CLOSURELIB_URL = https://github.com/google/closure-library.git
REPO_CLOSURELIB_MIRROR = /home/per/repositories/closure-library.git
REPO_CLOSURELIB_VER = v20180910

REPOS += $(BD)/protobuf
REPO_PROTOBUF_URL = https://github.com/protocolbuffers/protobuf
REPO_PROTOBUF_MIRROR = /home/per/repositories/protobuf.git
REPO_PROTOBUF_VER = v3.15.2

WEBSRCS += $(wildcard $(TOP)/src/mi-dashboard/*)

MISSRCS += $(TOP)/src/mi-simulator/mi-simulator.c
MISSRCS += $(PROTOBUFS_C)
MISINCS += $(BD)/libwebsockets/build/include
MISINCS += $(BD)/protobufs
MISLIBS += $(BD)/libwebsockets/build/lib/libwebsockets.so
MISDEPS += $(TOP)/src/mi-simulator/protocol_mi.c

PROTOBUFS += $(TOP)/src/protobufs/mi.proto
PROTOBUFS_C = $(foreach proto, $(PROTOBUFS), $(BD)/protobufs/$(shell basename $(proto) .proto).pb-c.c)
PROTOBUFS_JS = $(BD)/html/mi_protos.js

DESTDIR ?=
PREFIX ?= /usr

all: $(BD)/html/mi.html $(BD)/mi-simulator/mi-simulator

$(BD)/libwebsockets/build/lib/libwebsockets.so:
	mkdir -p $(BD)
	git -C $(BD) clone $(REPO_LWS_URL)
	git -C $(BD)/libwebsockets checkout $(REPO_LWS_VER)
	mkdir -p $(BD)/libwebsockets/build
	cd $(BD)/libwebsockets/build && cmake ..
	cd $(BD)/libwebsockets/build && make -j 88

$(BD)/html/two/two.min.js:
	mkdir -p $(BD)/html
	git -C $(BD) clone $(REPO_TWOJS_URL)
	git -C $(BD)/two.js checkout $(REPO_TWOJS_VER)
	mkdir -p $(BD)/html/two
	cd $(BD)/html && ln -s ../../two.js/build/two.min.js $@

$(BD)/html/closure:
	mkdir -p $(BD)/html
	git -C $(BD) clone $(REPO_CLOSURELIB_URL)
	git -C $(BD)/closure-library checkout $(REPO_CLOSURELIB_VER)
	cd $(BD)/html && ln -s ../closure-library/closure .
	
$(BD)/html/protobuf-js:
	mkdir -p $(BD)/html
	git -C $(BD) clone $(REPO_PROTOBUF_URL)
	git -C $(BD)/protobuf checkout $(REPO_PROTOBUF_VER)
	cd $(BD)/html && ln -s ../protobuf/js $@

$(BD)/html/mi.html: $(WEBSRCS) $(PROTOBUFS_JS) $(BD)/html/two/two.min.js $(BD)/html/closure $(BD)/html/protobuf-js
	mkdir -p $(BD)/html
	cp $(WEBSRCS) $(BD)/html

$(BD)/mi-simulator/mi-simulator: $(MISSRCS) $(MISLIBS) $(MISDEPS) $(PROTOBUFS_C)
	mkdir -p $(shell dirname $@)
	$(CC) $(foreach inc, $(MISINCS), -I$(inc)) $(MISSRCS) -o $@ -lprotobuf-c -lpthread -lm \
		$(foreach lib, $(MISLIBS), -L$(shell dirname $(lib)) -l$(shell basename $(lib) .so | cut -c 4-))

repos: $(REPOS)

$(PROTOBUFS_JS): $(PROTOBUFS)
	mkdir -p $(BD)/html
	protoc --js_out=library=mi_protos:$(BD)/html --proto_path=$(TOP)/src/protobufs $(PROTOBUFS)

$(PROTOBUFS_C): $(PROTOBUFS)
	mkdir -p $(BD)/protobufs
	cd $(TOP)/src/protobufs && protoc-c --c_out=$(BD)/protobufs $(shell basename $@ .pb-c.c).proto

debug:
	echo $(PROTOBUFS_JS)
	echo $(PROTOBUFS_C)

clean:
	$(RM) -r $(BD)

test-mi-dashboard: $(BD)/html/mi.html
	echo "browse to http://localhost:8000/mi.html"
	cd $(BD)/html && python -m RangeHTTPServer > $(BD)/webserver.log 2>&1

test-mi-simulator: $(BD)/mi-simulator/mi-simulator
	LD_LIBRARY_PATH=$(BD)/libwebsockets/build/lib $(BD)/mi-simulator/mi-simulator

test-client:
	firefox localhost:8000

install-mi-dashboard: $(BD)/html
	mkdir -p $(DESTDIR)$(PREFIX)/mi-dashboard
	tar -C $(BD)/html -h -cf - . | tar -C $(DESTDIR)$(PREFIX)/mi-dashboard -xf -

install-mi-simulator: $(BD)/mi-simulator/mi-simulator
	mkdir -p $(DESTDIR)$(PREFIX)/bin
	cp $(BD)/mi-simulator/mi-simulator $(DESTDIR)$(PREFIX)/bin

install: install-mi-dashboard install-mi-simulator

mi-container-image:
	rsync build/mi-simulator/mi-simulator test-container
	rm -rf test-container/html
	cp -a build/html test-container
	rm test-container/html/closure
	rm test-container/html/protobuf-js
	rm test-container/html/two/two.min.js
	cp -a build/closure-library/closure/ test-container/html/closure/
	cp -a build/protobuf/js/ test-container/html/protobuf-js/
	cp -a build/two.js/build/two.min.js test-container/html/two/two.min.js
	docker build --no-cache -t mi:v1 test-container

mi-container:
	docker run -it --rm --name mi \
		--publish 5080:80 \
		mi:v1
#		--entrypoint=bash \
#
